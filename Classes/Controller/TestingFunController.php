<?php
namespace AKT\AktTestingFun\Controller;

/**
 * TestingFunController
 */
class TestingFunController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{


    function findUniq($a) {
        return 'You have to find the unique';
    }

    function arrayDiff($a, $b) {
        return 'You have to find the difference';
    }

    function likes($names) {
        return 'You have to implement who likes this';
    }

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {

        $results['likes']['no one likes this'] = $this->likes([]);
        $results['likes']['Vitali likes this'] = $this->likes(["Vitali"]);
        $results['likes']['Vinh and Alex like this'] = $this->likes(["Vinh", "Alex"]);
        $results['likes']['Max, John and Marko like this'] = $this->likes(["Max", "John", "Marko"]);
        $results['likes']['Alex, Vinh and 2 others like this'] = $this->likes(["Alex", "Vinh", "Marko", "Max"]);


        $results['findUniq']['2'] = $this->findUniq([1, 1, 1, 2, 1, 1]);
//        $results['findUniq']['0.55'] = $this->findUniq([0, 0, 0.55, 0, 0]);

        $results['arrayDiff']['[2]'] = $this->arrayDiff([1,2],[1]);
//        $results['arrayDiff']['[1,3]'] = $this->arrayDiff([1,2,2,2,3],[2]);


        $this->view->assign('results', $results);

    }


}
