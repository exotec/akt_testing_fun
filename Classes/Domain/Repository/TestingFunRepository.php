<?php
namespace AKT\AktTestingFun\Domain\Repository;


/***
 *
 * This file is part of the "AKT - Testing Fun" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/
/**
 * The repository for TestingFuns
 */
class TestingFunRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
}
