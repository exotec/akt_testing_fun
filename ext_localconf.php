<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'AKT.AktTestingFun',
            'Pi1',
            [
                'TestingFun' => 'list'
            ],
            // non-cacheable actions
            [
                'TestingFun' => 'list'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    pi1 {
                        iconIdentifier = akt_testing_fun-plugin-pi1
                        title = LLL:EXT:akt_testing_fun/Resources/Private/Language/locallang_db.xlf:tx_akt_testing_fun_pi1.name
                        description = LLL:EXT:akt_testing_fun/Resources/Private/Language/locallang_db.xlf:tx_akt_testing_fun_pi1.description
                        tt_content_defValues {
                            CType = list
                            list_type = akttestingfun_pi1
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'akt_testing_fun-plugin-pi1',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:akt_testing_fun/Resources/Public/Icons/user_plugin_pi1.svg']
			);
		
    }
);
