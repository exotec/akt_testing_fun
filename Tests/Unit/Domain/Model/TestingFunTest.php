<?php
namespace AKT\AktTestingFun\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Alexander Weber <weber@exotec.de>
 */
class TestingFunTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \AKT\AktTestingFun\Domain\Model\TestingFun
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \AKT\AktTestingFun\Domain\Model\TestingFun();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }
}
