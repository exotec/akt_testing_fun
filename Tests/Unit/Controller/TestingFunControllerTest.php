<?php
namespace AKT\AktTestingFun\Tests\Unit\Controller;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Test case.
 *
 * @author Alexander Weber <weber@exotec.de>
 */
class TestingFunControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{

    /**
     * @var \AKT\AktTestingFun\Controller\TestingFunController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\AKT\AktTestingFun\Controller\TestingFunController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }



####################################################################################################
####################################################################################################
####################################################################################################


    /**
     * @param array $expectedResult
     * @dataProvider DataForLikeFunction
     * @test
     */
    public function testLikeFunction($expectedResult, $expected)
    {
//        print_r($expectedResult);
        $actual = $this->subject->likes($expectedResult);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    public function DataForLikeFunction()
    {
        return [
            '[]' => [[], 'no one likes this'],
            '["Peter"]' => [["Peter"], 'Peter likes this'],
            '["Jacob", "Alex"]' => [["Jacob", "Alex"], 'Jacob and Alex like this'],
            '["Max", "John", "Mark"]' => [["Max", "John", "Mark"], 'Max, John and Mark like this'],
            '["Alex", "Jacob", "Mark", "Max"]' => [["Alex", "Jacob", "Mark", "Max"], 'Alex, Jacob and 2 others like this']
        ];
    }


####################################################################################################
####################################################################################################
####################################################################################################



    /**
     * @param array $expectedResult
     * @dataProvider DataForFindUniqFunction
     * @test
     */
    public function testFindUniqFunction($expectedResult, $expected)
    {
        $actual = $this->subject->findUniq($expectedResult);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    public function DataForFindUniqFunction()
    {
        return [
            '[1, 1, 1, 2, 1, 1]' => [[1, 1, 1, 2, 1, 1], 2],
            '[0, 0, 0.55, 0, 0]' => [[0, 0, 0.55, 0, 0], 0.55],
//            '[0, 1, 0]' => [[0, 1, 0], 1],
//            '[4,4,4,3,4,4,4,4]' => [[4,4,4,3,4,4,4,4], 3],
//            '[-11,-11,-33,-23,-33,-233,-233]' => [[-11,-11,-33,-23,-33,-233,-233], -23],
//            '[2.34, 2.34, 1.23, 0.5, 0.3, 0.5, 0.3]' => [[2.34, 2.34, 1.23, 0.5, 0.3, 0.5, 0.3], 1.23]
        ];
    }


####################################################################################################
####################################################################################################
####################################################################################################


    /**
     * @param array $expectedResult
     * @dataProvider DataForArrayDiffFunction
     * @test
     */
    public function testArrayDiffFunction($expectedResult, $a, $b)
    {
        $actual = $this->subject->arrayDiff($a, $b);
        $this->assertEquals($expectedResult, $actual);
    }

    /**
     * @return array
     */
    public function DataForArrayDiffFunction()
    {
        return [
            '[1,2], [1]' => [[2], [1,2], [1]],
            '[2,2], [1,2,2]' => [[2,2], [1,2,2], [1]],
//            '[1,2,2], [1]' => [[1], [1,2,2], [2]],
//            '[1,2,2], []' => [[1,2,2], [1,2,2], []],
//            '[], [1,2]' => [[], [], [1,2]],
        ];
    }


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


//
//    /**
//     * @test
//     */
//    public function listActionFetchesAllTestingFunsFromRepositoryAndAssignsThemToView()
//    {
//
//        $allTestingFuns = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
//            ->disableOriginalConstructor()
//            ->getMock();
//
//        $testingFunRepository = $this->getMockBuilder(\AKT\AktTestingFun\Domain\Repository\TestingFunRepository::class)
//            ->setMethods(['findAll'])
//            ->disableOriginalConstructor()
//            ->getMock();
//        $testingFunRepository->expects(self::once())->method('findAll')->will(self::returnValue($allTestingFuns));
//        $this->inject($this->subject, 'testingFunRepository', $testingFunRepository);
//
//        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
//        $view->expects(self::once())->method('assign')->with('testingFuns', $allTestingFuns);
//        $this->inject($this->subject, 'view', $view);
//
//        $this->subject->listAction();
//    }
//
//    /**
//     * @test
//     */
//    public function showActionAssignsTheGivenTestingFunToView()
//    {
//        $testingFun = new \AKT\AktTestingFun\Domain\Model\TestingFun();
//
//        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
//        $this->inject($this->subject, 'view', $view);
//        $view->expects(self::once())->method('assign')->with('testingFun', $testingFun);
//
//        $this->subject->showAction($testingFun);
//    }
}
