
## The challanges

### 1. Set like button labels 
You probably know the "like" system from Facebook and other pages. People can "like" blog posts, pictures or other items. We want to create the text that should be displayed next to such an item.

Complete this function in your controller: 
    
    TestingFunController::likes()
    
The function must take in input array, containing the names of people who like an item. It must return the display text as shown in the examples:

    likes [] // must be "no one likes this"
    likes ["Peter"] // must be "Peter likes this"
    likes ["Jacob", "Alex"] // must be "Jacob and Alex like this"
    likes ["Max", "John", "Mark"] // must be "Max, John and Mark like this"
    likes ["Alex", "Jacob", "Mark", "Max"] // must be "Alex, Jacob and 2 others like this"

### 2. Find unique in array 
There is an array with some numbers. All numbers are equal except for one. It’s guaranteed that array contains more than 3 numbers. Try to find it!

    findUniq([ 1, 1, 1, 2, 1, 1 ]) === 2
    findUniq([ 0, 0, 0.55, 0, 0 ]) === 0.55

Complete this function in your controller: 
    
    TestingFunController::findUniq()
    
### 3. Find unique in array 
Your goal in this kata is to implement a difference function, which subtracts one list from another and returns the result.

It should remove all values from list a, which are present in list b.

    arrayDiff([1,2],[1]) == [2]
    
f a value is present in b, all of its occurrences must be removed from the other:

    arrayDiff([1,2,2,2,3],[2]) == [1,3]

Complete this function in your controller: 
    
    TestingFunController::arrayDiff()
    
    
NEW STUFF
=====
    
echo alphabet_position("The sunset sets at twelve o' clock.The narwhal bacons at midnight.=-='");

function alphabet_position(string $s): string {
    $alphas = range('A', 'Z');
    $chars = str_split(strToUpper($s));
    
    foreach( $chars as $k => $char) {
        if( in_array($char, $alphas)) {
            $alphaKeys[$k] = array_search ($char, $alphas) + 1;   
        }
    }
    
    if( is_array($alphaKeys) )
       return implode(' ', $alphaKeys);
    return false;
}

/*
function alphabet_position(string $s):string {
  return implode(' ', array_filter(array_map(function($x){
    return array_search($x, str_split('_abcdefghijklmnopqrstuvwxyz'));}, str_split(strtolower($s)))));
}
*/