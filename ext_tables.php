<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'AKT.AktTestingFun',
            'Pi1',
            'Testing Fun Plugin'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('akt_testing_fun', 'Configuration/TypoScript', 'AKT - Testing Fun');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_akttestingfun_domain_model_testingfun', 'EXT:akt_testing_fun/Resources/Private/Language/locallang_csh_tx_akttestingfun_domain_model_testingfun.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_akttestingfun_domain_model_testingfun');

    }
);
